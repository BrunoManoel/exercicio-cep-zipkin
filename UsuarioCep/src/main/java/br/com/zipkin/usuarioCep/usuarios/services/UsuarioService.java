package br.com.zipkin.usuarioCep.usuarios.services;

import br.com.zipkin.usuarioCep.usuarios.DTOs.UsuarioDTO;
import br.com.zipkin.usuarioCep.usuarios.models.Cep;
import br.com.zipkin.usuarioCep.usuarios.clients.CepClient;
import br.com.zipkin.usuarioCep.usuarios.models.Usuario;
import br.com.zipkin.usuarioCep.usuarios.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    CepClient cepClient;


    @NewSpan(name = "consultacep-service")
    private Cep getCep(String cep){
        try{
            return cepClient.getCep(cep);
        } catch (RuntimeException e) {
            throw new RuntimeException("CEP digitado não existe");
        }

    }

    @NewSpan(name = "criausuario-service")
    public Usuario criaUsuarioParaCep(@SpanTag("usuario")UsuarioDTO usuarioDTO, @SpanTag("cep_pesquisado")String cep) {

        Cep cepPesquisado = getCep(cep);

        Optional<Usuario> usuarioInserido = usuarioRepository.findByCep(cepPesquisado);
        if(usuarioInserido.isPresent()){
            throw new RuntimeException("Usuário " + usuarioDTO.getNome() + " já possui o CEP " + cep + " atrelado");
        }

        Usuario novoUsuario = new Usuario();
        novoUsuario.setCep(cepPesquisado);
        novoUsuario.setNome(usuarioDTO.getNome());

        return usuarioRepository.save(novoUsuario);
    }
}
