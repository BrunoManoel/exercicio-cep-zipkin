package br.com.zipkin.usuarioCep.usuarios.controllers;

import br.com.zipkin.usuarioCep.usuarios.DTOs.UsuarioDTO;
import br.com.zipkin.usuarioCep.usuarios.models.Usuario;
import br.com.zipkin.usuarioCep.usuarios.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("usuario")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @PostMapping("/{cep}")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Usuario criaUsuarioParaOCep(@RequestBody UsuarioDTO usuarioDTO, @PathVariable(name = "cep") String cep) {
        return usuarioService.criaUsuarioParaCep(usuarioDTO, cep);
    }

}
