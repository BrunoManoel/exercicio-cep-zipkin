package br.com.zipkin.consultaCep.consultas.controllers;

import br.com.zipkin.consultaCep.consultas.models.Cep;
import br.com.zipkin.consultaCep.consultas.services.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("cep")
public class CepController {

    @Autowired
    CepService cepService;

    @GetMapping("/{cep}")
    public Cep create(@PathVariable String cep) {
        return cepService.getByCep(cep);
    }


}
